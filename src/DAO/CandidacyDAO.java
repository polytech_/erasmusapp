package DAO;

import Model.Candidacy;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import java.util.ArrayList;
import java.util.List;

public class CandidacyDAO extends DAO<Candidacy> {

    public CandidacyDAO(EntityManager entityManager){
        super(entityManager);
    }

    @Override
    public boolean add(Candidacy element) {
        try {
            entityManager.persist(element);
            System.out.println("Add successful !");
        }catch (Exception e){
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public boolean modify(Candidacy element) {
        try {
            entityManager.merge(element);
            if (entityManager.contains(element)) {
                System.out.println("Modify successful !");
                return true;
            } else {
                return false;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }


    @Override
    public Candidacy getObjectById(int id) {
        try{
            return entityManager.find(Candidacy.class,id);
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public List<Candidacy> getAll() {
        List<Candidacy> candidacies;
        try{
            TypedQuery<Candidacy> query = entityManager.
                    createQuery("SELECT candidacy FROM Candidacy candidacy ", Candidacy.class);
            candidacies = query.getResultList();
            return candidacies;
        }catch (Exception e){
            e.printStackTrace();
        }

        return null;
    }

}

