package DAO;

import Model.Student;
import Model.Teaching;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.List;

public class TeachingDAO extends DAO<Teaching> {

    public TeachingDAO(EntityManager entityManager) {
        super(entityManager);
    }



    @Override
    public boolean add(Teaching element) {
        try {
            entityManager.persist(element);
            System.out.println("Add successful !");
        }catch (Exception e){
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public boolean modify(Teaching element) {
        try {
            entityManager.merge(element);
            if (entityManager.contains(element)) {
                return true;
            } else {
                return false;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public Teaching getObjectById(int id) {
        try{
            Teaching teaching = entityManager.find(Teaching.class,id);
            return teaching;
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }


    @Override
    public List<Teaching> getAll() {
        List<Teaching> teachings;
        try{
            Query query = entityManager.createQuery("SELECT teaching.name," +
                    "teaching.numCredit,teaching.numHours FROM Teaching teaching ");
            teachings = query.getResultList();
            return teachings;
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

    public List<String> getAllNames() {
        List<String> names;
        try{
            Query query = entityManager.createQuery("SELECT teaching.name FROM Teaching teaching ");
            names = query.getResultList();
            return names;
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

    public Teaching getObjectByName(String name) {
        Teaching result;
        try{
            Query query = entityManager.createQuery("SELECT teaching.idTeaching FROM Teaching teaching " +
                    "WHERE teaching.name='"+name+"'");
            int id = (int)query.getSingleResult();
            result = entityManager.find(Teaching.class, id);
            return result;
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;


    }


}
