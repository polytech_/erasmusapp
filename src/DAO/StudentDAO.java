package DAO;

import Model.Student;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.List;

public class StudentDAO extends DAO<Student> {


    public StudentDAO(EntityManager entityManager) {
        super(entityManager);
    }

    @Override
    public boolean add(Student element) {
        try {
            entityManager.persist(element);
            System.out.println("Add successful !");
        }catch (Exception e){
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public boolean modify(Student element) {
        try {
            entityManager.merge(element);
            if (entityManager.contains(element)) {
                return true;
            } else {
                return false;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public Student getObjectById(int id) {
        try{
            Student student = entityManager.find(Student.class,id);
            return student;
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }


    public Student getObjectByName(String lastName) {
        Student result;
        try{
            Query query = entityManager.createQuery("SELECT student.personNumber FROM Student student where student.lastName='"+lastName+"'");
            int id = (int)query.getSingleResult();
            return entityManager.find(Student.class, id);
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public List<Student> getAll() {
        List<Student> students;
        try{
            Query query = entityManager.createQuery("SELECT student.lastName," +
                    "student.firstName, student.semestreScore, student.personNumber FROM Student student ");

            students = query.getResultList();
            return students;
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

    public List<String> getAllNames() {
        List<String> names;
        try{
            Query query = entityManager.createQuery("SELECT CONCAT(student.lastName, ' ', student.firstName)  FROM Student student");
            names = query.getResultList();
            return names;
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }
}
