package DAO;


import Model.ErasmusManager;
import Model.LocalErasmusManager;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.List;

public class LocalErasmusManagerDAO extends DAO<LocalErasmusManager> {

    public LocalErasmusManagerDAO(EntityManager entityManager) {
        super(entityManager);
    }

    @Override
    public boolean add(LocalErasmusManager element) {
        try {
            entityManager.persist(element);
            System.out.println("Add successful !");
        }catch (Exception e){
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public boolean modify(LocalErasmusManager element) {
        try {
            entityManager.merge(element);
            if (entityManager.contains(element)) {
                return true;
            } else {
                return false;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public LocalErasmusManager getObjectById(int id) {
        try{
            LocalErasmusManager localErasmusManager = entityManager.find(LocalErasmusManager.class,id);
            return localErasmusManager;
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }


    public Integer getIdByName(String fname, String lname) {
        Integer id;
        try{
            Query query = entityManager.createQuery("SELECT lem.personNumber " +
                    "FROM LocalErasmusManager lem " +
                    "WHERE lem.firstName = "+fname+" and lem.lastName = "+lname);

            id = (Integer) query.getSingleResult();
            return id;
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public List<LocalErasmusManager> getAll() {
        List<LocalErasmusManager> localErasmusManagers;
        try{
            Query query = entityManager.createQuery("SELECT LocalErasmusManager.lastName," +
                    "LocalErasmusManager.firstName, LocalErasmusManager.scholarship FROM LocalErasmusManager");
            localErasmusManagers = query.getResultList();
            return localErasmusManagers;
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

    public List<String> getAllNames() {
        List<String> names;
        try{
            Query query = entityManager.createQuery("SELECT CONCAT(localerasmusmanager.lastName, ' ', localerasmusmanager.firstName)  FROM LocalErasmusManager localerasmusmanager");
            names = query.getResultList();
            return names;
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

}
