package DAO;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

public abstract class DAO<T> {

    @PersistenceContext
    protected EntityManager entityManager;

    public DAO(EntityManager entityManager){this.entityManager = entityManager;}

    public abstract boolean add(T element);

    public abstract boolean modify(T element);

    public abstract T getObjectById(int id);

    public abstract List<T> getAll();

}
