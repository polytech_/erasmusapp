package DAO;


import Model.ErasmusManager;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.List;

public class ErasmusManagerDAO extends DAO<ErasmusManager> {

    public ErasmusManagerDAO(EntityManager entityManager) {
        super(entityManager);
    }

    @Override
    public boolean add(ErasmusManager element) {
        try {
            entityManager.persist(element);
            System.out.println("Add successful !");
        }catch (Exception e){
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public boolean modify(ErasmusManager element) {
        try {
            entityManager.merge(element);
            if (entityManager.contains(element)) {
                System.out.println("Modify Successful !");
                return true;
            } else {
                return false;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public ErasmusManager getObjectById(int id) {
        try{
            ErasmusManager erasmusManager = entityManager.find(ErasmusManager.class,id);
            return erasmusManager;
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public List<ErasmusManager> getAll() {
        List<ErasmusManager> erasmusManagers;
        try{
            Query query = entityManager.createQuery("SELECT ErasmusManager.lastName," +
                    "ErasmusManager.firstName FROM ErasmusManager ");
            erasmusManagers = query.getResultList();
            return erasmusManagers;
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

}
