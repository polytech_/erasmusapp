package DAO;

import Model.Candidacy;
import Model.Scholarship;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.List;

public class ScholarShipDAO extends DAO<Scholarship> {

    public ScholarShipDAO(EntityManager entityManager){
        super(entityManager);
    }

    @Override
    public boolean add(Scholarship element) {
        try {
            entityManager.persist(element);
            System.out.println("Add successful !");
        }catch (Exception e){
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public boolean modify(Scholarship element) {
        try {
            entityManager.merge(element);
            if (entityManager.contains(element)) {
                return true;
            } else {
                return false;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public Scholarship getObjectById(int id) {
        try{
            Scholarship scholarship = entityManager.find(Scholarship.class,id);
            return scholarship;
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public List<Scholarship> getAll() {
        List<Scholarship> scholarships;
        try{
            Query query = entityManager.createQuery("SELECT Scholarship.destination,Scholarship.nbPlaces FROM Scholarship ");
            scholarships = query.getResultList();
            return scholarships;
        }catch (Exception e){
            e.printStackTrace();
        }

        return null;
    }

    public List<Integer> getAllIds() {
        List<Integer> ids;
        try{
            Query query = entityManager.createQuery("SELECT scholarship.idScholarship FROM Scholarship scholarship");
            ids = query.getResultList();
            return ids;
        }catch (Exception e){
            e.printStackTrace();
        }

        return null;
    }


}
