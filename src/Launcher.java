
import Controller.MainPageController;
import Model.*;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Launcher extends Application {


    @Override
    public void start(Stage primaryStage) throws IOException {


        //Initialisation de l'entityManager
        EntityManagerFactory entityManagerFactory =
                Persistence.createEntityManagerFactory("test");

        EntityManager entityManager = entityManagerFactory.createEntityManager();

        //initialize(entityManager);

        FXMLLoader loader = new FXMLLoader(getClass().getResource("/MainPage.fxml"));
        Parent root = loader.load();

        MainPageController mainPageController = loader.<MainPageController>getController();
        mainPageController.setEntityManager(entityManager);

        primaryStage.setScene(new Scene(root));
        primaryStage.setTitle("Home");
        primaryStage.setResizable(false);
        primaryStage.show();

    }

    public void initialize(EntityManager entityManager) {

        entityManager.getTransaction().begin();

        Student student1 = new Student("FName1", "LName1", 12);
        Student student2 = new Student("FName2", "LName2", 9);
        Student student3 = new Student("FName3", "LName3", 16);
        Student student4 = new Student("FName4", "LName4", 13);
        Student student5 = new Student("FName5", "LName5", 14);

        ErasmusManager em = new ErasmusManager("FEM", "LEM");
        LocalErasmusManager lem = new LocalErasmusManager("FLEM", "LLEM");

        Teaching teaching1 = new Teaching("Teaching1", 5, 20);
        Teaching teaching2 = new Teaching("Teaching2", 8, 25);
        Teaching teaching3 = new Teaching("Teaching3", 3, 13);
        Teaching teaching4 = new Teaching("Teaching4", 10, 15);
        Teaching teaching5 = new Teaching("Teaching5", 8, 10);
        Teaching teaching6 = new Teaching("Teaching6", 6, 12);
        Teaching teaching7 = new Teaching("Teaching7", 7, 17);
        Teaching teaching8 = new Teaching("Teaching8", 12, 18);
        Teaching teaching9 = new Teaching("Teaching9", 6, 12);
        Teaching teaching10 = new Teaching("Teaching10", 11, 15);


        ArrayList<Teaching> teachings = new ArrayList<>();
        teachings.add(teaching10);
        teachings.add(teaching6);
        teachings.add(teaching7);

        ArrayList<Teaching> teachings1 = new ArrayList<>();
        teachings.add(teaching5);
        teachings.add(teaching1);
        teachings.add(teaching7);

        ArrayList<Teaching> teachings2 = new ArrayList<>();
        teachings.add(teaching10);
        teachings.add(teaching8);
        teachings.add(teaching3);

        ArrayList<Teaching> teachings3 = new ArrayList<>();
        teachings.add(teaching2);
        teachings.add(teaching5);
        teachings.add(teaching6);

        Scholarship scholarship1 = new Scholarship("dest1", 200, teachings);
        Scholarship scholarship2 = new Scholarship("dest1", 150, teachings2);
        Scholarship scholarship3 = new Scholarship("dest1", 690, teachings3);
        Scholarship scholarship4 = new Scholarship("dest1", 120, teachings1);
        Scholarship scholarship5 = new Scholarship("dest1", 300, teachings3);

        student1.addScolarship(scholarship1);
        student1.addScolarship(scholarship2);

        student2.addScolarship(scholarship3);

        student4.addScolarship(scholarship4);
        student4.addScolarship(scholarship4);




        entityManager.persist(teaching1);
        entityManager.persist(teaching2);
        entityManager.persist(teaching3);
        entityManager.persist(teaching4);
        entityManager.persist(teaching5);
        entityManager.persist(teaching6);
        entityManager.persist(teaching7);
        entityManager.persist(teaching8);
        entityManager.persist(teaching9);
        entityManager.persist(teaching10);

        entityManager.persist(scholarship1);
        entityManager.persist(scholarship2);
        entityManager.persist(scholarship3);
        entityManager.persist(scholarship4);
        entityManager.persist(scholarship5);

        entityManager.persist(student1);
        entityManager.persist(student2);
        entityManager.persist(student3);
        entityManager.persist(student4);
        entityManager.persist(student5);

        entityManager.persist(em);
        entityManager.persist(lem);


        entityManager.getTransaction().commit();

    }
}
