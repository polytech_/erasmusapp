package Model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@PrimaryKeyJoinColumn
public class Student extends Person {

    private int semestreScore;

    @OneToOne(mappedBy = "student", cascade = CascadeType.ALL)
    private Candidacy candidacy;


    @ManyToMany
    @JoinTable(
            name="student_scholarship",
            joinColumns=@JoinColumn(name="student_id", referencedColumnName="personNumber"),
            inverseJoinColumns=@JoinColumn(name="scolarship_id", referencedColumnName="idScholarship"))
    private List<Scholarship> listScholarships;

    public Student() {
        listScholarships = new ArrayList<>();
    }

    public Student(String firstName, String lastName, int grade) {
        super(firstName, lastName);
        this.setScore(grade);
        listScholarships = new ArrayList<>();
    }

    public int getScore() {
        return semestreScore;
    }

    public void setScore(int grade) {
        this.semestreScore = grade;
    }

    public Candidacy getCandidacy() {
        return candidacy;
    }

    public void setCandidacy(Candidacy candidacy) {
        this.candidacy = candidacy;
    }

    public List<Scholarship> getListScholarships() {
        return listScholarships;
    }

    public void setListScholarships(ArrayList<Scholarship> listScholarships) {
        this.listScholarships = listScholarships;
    }

    public void addScolarship(Scholarship scholarship) {
        listScholarships.add(scholarship);
    }
}
