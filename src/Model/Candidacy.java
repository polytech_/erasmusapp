package Model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Candidacy {

    @Id
    @GeneratedValue (strategy = GenerationType.IDENTITY)
    private int reference;

    //Limiter la taille à 2

    @ManyToMany
    @JoinTable(
            name="candidacy_scholarship",
            joinColumns=@JoinColumn(name="candidacy_id", referencedColumnName="reference"),
            inverseJoinColumns=@JoinColumn(name="scolarship_id", referencedColumnName="idScholarship"))
    private List<Scholarship> listScholarship;

    private String status;

    private Integer score;

    @OneToMany(mappedBy = "scholarship")
    private List<Teaching> listTeaching;

    @OneToOne
    private Student student;


    @PrePersist
    public void prePersist() {
        if(score == null)
            score = -1;
        status = "Non notée";

    }

    public Candidacy(){

        listScholarship = new ArrayList<>();

    }




    public Candidacy(int reference, Integer score, String status, String studentName) {
        this.reference = reference;
        this.score = score;
        this.status = status;
        this.student.setLastName(studentName);
    }




    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Integer getScore() {
        return score;
    }

    public void setScore(Integer score) {
        this.score = score;
    }

    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }

    public String getStudentName() {
        return student.getLastName();
    }

    public void setStudentName(String name) {
        this.student.setLastName(name);
    }

    public int getReference() {
        return reference;
    }

    public void setReference(int reference) {
        this.reference = reference;
    }

    public void setListScholarship(List<Scholarship> listScholarship) {
        this.listScholarship = listScholarship;
    }

    public List<Scholarship> getListScholarship() {
        return listScholarship;
    }

    public void setListTeaching(List<Teaching> listTeaching) {
        this.listTeaching = listTeaching;
    }

    public List<Teaching> getListTeaching() {
        return listTeaching;
    }

    public void addScolarship(Scholarship scholarship) {
        listScholarship.add(scholarship);
    }
}
