package Model;

import org.hibernate.annotations.Cascade;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Scholarship {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int idScholarship;

    private String destination;
    private int nbPlaces;

    @OneToMany(mappedBy = "scholarship")
    private List<Teaching> listTeaching;

    @ManyToMany(mappedBy = "listScholarships",cascade = CascadeType.ALL)
    private List<Student> listStudents;

    @ManyToMany(mappedBy = "listScholarship",cascade = CascadeType.ALL)
    private List<Candidacy> listCandidacy;


    @OneToOne(mappedBy = "scholarship")
    private LocalErasmusManager localErasmusManager;


    public Scholarship(){

    }

    public Scholarship(String destination, int nbPlaces, List<Teaching> teachings) {
        this.destination = destination;
        this.nbPlaces = nbPlaces;
        this.listTeaching = teachings;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public int getNbPlaces() {
        return nbPlaces;
    }

    public void setNbPlaces(int nbPlaces) {
        this.nbPlaces = nbPlaces;
    }

    public int getIdScholarship() {
        return idScholarship;
    }

    public void setIdScholarship(int idScholarship) {
        this.idScholarship = idScholarship;
    }

    public List<Student> getListStudents() {
        return listStudents;
    }

    public void setListStudents(List<Student> listStudents) {
        this.listStudents = listStudents;
    }

    public List<Candidacy> getListCandidacy() {
        return listCandidacy;
    }

    public void setListCandidacy(List<Candidacy> listCandidacy) {
        this.listCandidacy = listCandidacy;
    }

    public List<Teaching> getListTeaching() {
        return listTeaching;
    }

    public void setListTeaching(List<Teaching> listTeaching) {
        this.listTeaching = listTeaching;
    }

    public LocalErasmusManager getLocalErasmusManager() {
        return localErasmusManager;
    }

    public void setLocalErasmusManager(LocalErasmusManager localErasmusManager) {
        this.localErasmusManager = localErasmusManager;
    }

    public void addTeaching(Teaching teaching) {
        listTeaching.add(teaching);
    }
 }
