package Model;

import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import java.util.ArrayList;

@Entity
@PrimaryKeyJoinColumn
public class LocalErasmusManager extends Person {

    @OneToOne
    private Scholarship scholarship;

    public LocalErasmusManager() {
    }

    public LocalErasmusManager(String firstName, String lastName) {
        super(firstName, lastName);
    }

}
