package Model;

import javax.persistence.*;


@Entity
public class Teaching {

    @Id
    @GeneratedValue (strategy = GenerationType.IDENTITY)
    private int idTeaching;
    private String name;
    private int numCredit;
    private int numHours;


    @ManyToOne
    private Scholarship scholarship;

    @ManyToOne
    private Candidacy candidacy;

    public int getIdTeaching() {
        return idTeaching;
    }

    public void setIdTeaching(int idTeaching) {
        this.idTeaching = idTeaching;
    }

    public Teaching(String name, int numCredit, int numHours) {
        this.name = name;
        this.numCredit = numCredit;
        this.numHours = numHours;
    }

    public Teaching() {}

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getNumCredit() {
        return numCredit;
    }

    public void setNumCredit(int numCredit) {
        this.numCredit = numCredit;
    }

    public int getNumHours() {
        return numHours;
    }

    public void setNumHours(int numHours) {
        this.numHours = numHours;
    }

    public Candidacy getCandidacy() {
        return candidacy;
    }

    public void setCandidacy(Candidacy candidacy) {
        this.candidacy = candidacy;
    }

    public Scholarship getScholarship() {
        return scholarship;
    }

    public void setScholarship(Scholarship scholarship) {
        this.scholarship = scholarship;
    }

}