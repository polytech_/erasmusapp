package Model;

import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import java.util.ArrayList;

@Entity
@PrimaryKeyJoinColumn
public class ErasmusManager extends Person {

    public ErasmusManager() {
    }

    public ErasmusManager(String firstName, String lastName) {
        super(firstName, lastName);
    }

}
