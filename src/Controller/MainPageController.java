package Controller;

import Controller.Management.CandidacyManagement;

import Controller.Rating.RatingController;
import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.layout.Region;
import javafx.stage.Modality;
import javafx.stage.Stage;

import javax.persistence.EntityManager;
import java.io.IOException;

public class MainPageController {

    private EntityManager entityManager;

    public void setEntityManager(EntityManager entityManager) {
        this.entityManager = entityManager;
    }


    public void CandidacyManagement(ActionEvent event) throws IOException {

        Stage st = new Stage();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/Management/CandidacyManagement.fxml"));
        Region root = loader.load();

        CandidacyManagement candidacyManagement = loader.getController();
        candidacyManagement.setEntityManager(entityManager);
        candidacyManagement.init();

        //Récupération de la fenetre de login
        Node node = (Node) event.getSource();
        Stage stage =(Stage)node.getScene().getWindow();
        //Fermeture de celle-ci
        stage.close();

        Scene scene = new Scene(root);
        st.setScene(scene);
        st.initModality(Modality.APPLICATION_MODAL);
        st.setResizable(false);
        st.setTitle("Candidacy Management");
        st.show();
    }


    public void CandidacyRating(ActionEvent event) throws IOException {

        Stage st = new Stage();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/Management/CandidacyRating.fxml"));
        Region root = loader.load();

        RatingController ratingController = loader.getController();
        ratingController.setEntityManager(entityManager);
        ratingController.init();



        //Récupération de la fenetre de login
        Node node = (Node) event.getSource();
        Stage stage =(Stage)node.getScene().getWindow();
        //Fermeture de celle-ci
        stage.close();


        Scene scene = new Scene(root);
        st.setScene(scene);
        st.initModality(Modality.APPLICATION_MODAL);
        st.setResizable(false);
        st.setTitle("Candidacy Management");
        st.show();
    }


}
