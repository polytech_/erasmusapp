package Controller.Add;

import Controller.Management.CandidacyManagement;
import DAO.CandidacyDAO;
import DAO.ScholarShipDAO;
import DAO.StudentDAO;
import DAO.TeachingDAO;
import Model.Candidacy;
import Model.Scholarship;
import Model.Teaching;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.Region;
import javafx.stage.Modality;
import javafx.stage.Stage;

import javax.persistence.EntityManager;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class AddCandidacy {

    private EntityManager entityManager;

    public void setEntityManager(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    private CandidacyDAO candidacyDAO;
    private StudentDAO studentDAO;
    private ScholarShipDAO scholarShipDAO;
    private TeachingDAO teachingDAO;

    private CandidacyManagement candidacyManagement;

    public void setCandidacyManagement(CandidacyManagement candidacyManagement) {
        this.candidacyManagement = candidacyManagement;
    }

    @FXML
    public ComboBox studentNameComboBox;

    @FXML
    public ComboBox scholarshipNumberComboBox;

    @FXML
    public ComboBox firstScholarShipComboBox;

    @FXML
    public ComboBox secondScolarShipComboBox;


    @FXML
    private ComboBox<String> firstScholarShipTeaching1;
    @FXML
    private ComboBox<String> firstScholarShipTeaching2;
    @FXML
    private ComboBox<String> firstScholarShipTeaching3;


    @FXML
    private ComboBox<String> secondScholarShipTeaching1;
    @FXML
    private ComboBox<String> secondScholarShipTeaching2;
    @FXML
    private ComboBox<String> secondScholarShipTeaching3;




    public AddCandidacy() {

    }

    public void init(){
        this.candidacyDAO = new CandidacyDAO(entityManager);
        this.studentDAO = new StudentDAO(entityManager);
        this.scholarShipDAO = new ScholarShipDAO(entityManager);
        this.teachingDAO = new TeachingDAO(entityManager);

        //Intialisation des comboBox pour le(s) choix de bourse
        studentNameComboBox.setItems(FXCollections.observableArrayList(studentDAO.getAllNames()));
        scholarshipNumberComboBox.setItems(FXCollections.observableArrayList("1", "2"));
        firstScholarShipComboBox.setItems(FXCollections.observableArrayList(scholarShipDAO.getAllIds()));
        secondScolarShipComboBox.setItems(FXCollections.observableArrayList(scholarShipDAO.getAllIds()));

        firstScholarShipTeaching1.setItems(FXCollections.observableArrayList(teachingDAO.getAllNames()));
        firstScholarShipTeaching2.setItems(FXCollections.observableArrayList(teachingDAO.getAllNames()));
        firstScholarShipTeaching3.setItems(FXCollections.observableArrayList(teachingDAO.getAllNames()));

        secondScholarShipTeaching1.setItems(FXCollections.observableArrayList(teachingDAO.getAllNames()));
        secondScholarShipTeaching2.setItems(FXCollections.observableArrayList(teachingDAO.getAllNames()));
        secondScholarShipTeaching3.setItems(FXCollections.observableArrayList(teachingDAO.getAllNames()));



        //On Masque les comboBox
        firstScholarShipComboBox.setVisible(false);
        secondScolarShipComboBox.setVisible(false);

        firstScholarShipTeaching1.setVisible(false);
        firstScholarShipTeaching2.setVisible(false);
        firstScholarShipTeaching3.setVisible(false);

        secondScholarShipTeaching1.setVisible(false);
        secondScholarShipTeaching2.setVisible(false);
        secondScholarShipTeaching3.setVisible(false);

    }


    public void CandidacyManagement(ActionEvent event) {

        entityManager.getTransaction().begin();

        String firstScholarShip, secondScholarShip;
        boolean checkfscholarship, checksScholarship;
        String studentName = studentNameComboBox.getSelectionModel().getSelectedItem().toString();
        boolean checkName = studentNameComboBox.getSelectionModel().getSelectedItem().toString().isEmpty();
        String scholarshipNumber = scholarshipNumberComboBox.getSelectionModel().getSelectedItem().toString();
        boolean checkScolarShipNumber = scholarshipNumberComboBox.getSelectionModel().getSelectedItem().toString().isEmpty();

        List<String> firstteachingNames = new ArrayList<String>();


        firstteachingNames.add(firstScholarShipTeaching1.getSelectionModel().getSelectedItem());
        firstteachingNames.add(firstScholarShipTeaching2.getSelectionModel().getSelectedItem());
        firstteachingNames.add(firstScholarShipTeaching3.getSelectionModel().getSelectedItem());


        firstteachingNames.forEach(name -> teachingDAO.getObjectByName(name));
        List<Teaching> firstteachings = new ArrayList<Teaching>();
        for (String teachingName: firstteachingNames) {
            firstteachings.add(teachingDAO.getObjectByName(teachingName));
        }

        List<String> secondteachingNames = new ArrayList<String>();
        secondteachingNames.forEach(name -> teachingDAO.getObjectByName(name));
        List<Teaching> secondteachings = new ArrayList<Teaching>();
        for (String teachingName: secondteachingNames) {
            secondteachings.add(teachingDAO.getObjectByName(teachingName));
        }




        switch (scholarshipNumber){

            //Si une seule bourse est sélectionnée
            case "1" :

                firstScholarShip = firstScholarShipComboBox.getSelectionModel().getSelectedItem().toString();
                checkfscholarship = firstScholarShipComboBox.getSelectionModel().getSelectedItem().toString().isEmpty();
                if((!checkName) && (!checkScolarShipNumber) && (!checkfscholarship)){

                    //Initialisation
                    Candidacy candidacyToAdd = new Candidacy();

                    Scholarship scholarship = scholarShipDAO.getObjectById(Integer.parseInt(firstScholarShip));
                    //Ajout
                    candidacyToAdd.addScolarship(scholarship);

                    candidacyToAdd.setStudent(studentDAO.getObjectByName(studentName.split(" ")[0]));

                    candidacyDAO.add(candidacyToAdd);

                    candidacyManagement.getDonnees().add(candidacyToAdd);

                    //Fermeture de celle-ci
                    Node node = (Node) event.getSource();
                    Stage stage =(Stage)node.getScene().getWindow();
                    stage.close();
                }else{
                    //Alert Msg
                }
                break;


            //Si deux bourses sont sélectionnées
            case "2" :

                firstScholarShip = firstScholarShipComboBox.getSelectionModel().getSelectedItem().toString();
                checkfscholarship = firstScholarShipComboBox.getSelectionModel().getSelectedItem().toString().isEmpty();
                secondScholarShip = secondScolarShipComboBox.getSelectionModel().getSelectedItem().toString();
                checksScholarship = secondScolarShipComboBox.getSelectionModel().getSelectedItem().toString().isEmpty();


                if((!checkName) && (!checkScolarShipNumber) && (!checkfscholarship) && (!checksScholarship)){

                    //Initialisation
                    Candidacy candidacyToAdd = new Candidacy();

                    List<Teaching> teachings = new ArrayList<Teaching>();


                    candidacyToAdd.addScolarship(scholarShipDAO.getObjectById(Integer.parseInt(firstScholarShip)));
                    candidacyToAdd.addScolarship(scholarShipDAO.getObjectById(Integer.parseInt(secondScholarShip)));


                    candidacyToAdd.setListTeaching(firstteachings);
                    candidacyToAdd.setListTeaching(secondteachings);

                    candidacyToAdd.setStudent(studentDAO.getObjectByName(studentName.split(" ")[0]));
                    candidacyDAO.add(candidacyToAdd);


                    Node node = (Node) event.getSource();
                    Stage stage =(Stage)node.getScene().getWindow();
                    //Fermeture de celle-ci
                    stage.close();
                }else{
                    //Alert Msg
                }
                break;
        }
        entityManager.getTransaction().commit();

    }

    public void selectScholarShipNumber(ActionEvent event) {
        if(scholarshipNumberComboBox.getSelectionModel().getSelectedItem().equals("1")){
            firstScholarShipComboBox.setVisible(true);
            firstScholarShipTeaching1.setVisible(true);
            firstScholarShipTeaching2.setVisible(true);
            firstScholarShipTeaching3.setVisible(true);
            secondScolarShipComboBox.setVisible(false);
            secondScholarShipTeaching1.setVisible(false);
            secondScholarShipTeaching2.setVisible(false);
            secondScholarShipTeaching3.setVisible(false);
        }else if(scholarshipNumberComboBox.getSelectionModel().getSelectedItem().equals("2")){
            firstScholarShipComboBox.setVisible(true);
            firstScholarShipTeaching1.setVisible(true);
            firstScholarShipTeaching2.setVisible(true);
            firstScholarShipTeaching3.setVisible(true);
            secondScolarShipComboBox.setVisible(true);
            secondScholarShipTeaching1.setVisible(true);
            secondScholarShipTeaching2.setVisible(true);
            secondScholarShipTeaching3.setVisible(true);
        }
    }
}
