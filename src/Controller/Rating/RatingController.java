package Controller.Rating;

import Controller.MainPageController;
import DAO.CandidacyDAO;
import Model.Candidacy;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.Region;
import javafx.stage.Modality;
import javafx.stage.Stage;

import javax.persistence.EntityManager;
import java.io.IOException;

public class RatingController {

    private EntityManager entityManager;

    private CandidacyDAO candidacyDAO;

    private ObservableList<Candidacy> donnees;

    public ObservableList<Candidacy> getDonnees() {
        return donnees;
    }
    public void setDonnees(ObservableList<Candidacy> donnees) {
        this.donnees = donnees;
    }

    public void setEntityManager(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @FXML
    private TableColumn<Candidacy, String> StatusColumn;

    @FXML
    private TableColumn<Candidacy, String> stdNameColumn;

    @FXML
    private TableColumn<Candidacy, Integer> referenceColumn;

    @FXML
    private TableView<Candidacy> candidacyTableView;

    @FXML
    private TableColumn<Candidacy, String> ScoreColumn;

    public void init(){
        candidacyDAO = new CandidacyDAO(entityManager);

        referenceColumn.setCellValueFactory(new PropertyValueFactory<>("Reference"));
        ScoreColumn.setCellValueFactory(new PropertyValueFactory<>("Score"));
        StatusColumn.setCellValueFactory(new PropertyValueFactory<>("Status"));
        stdNameColumn.setCellValueFactory(new PropertyValueFactory<>("StudentName"));


        //Initialisation des données de la tableview
        setDonnees(FXCollections.observableArrayList(candidacyDAO.getAll()));

        candidacyTableView.setItems(donnees);

        candidacyTableView.setRowFactory(tv->{
            TableRow<Candidacy> row = new TableRow<>();
            row.setOnMouseClicked(event -> {
                //On récupère la ligne selectionnée
                if (event.getClickCount() == 2 && (! row.isEmpty()) ) {
                    Stage st = new Stage();
                    FXMLLoader loader = new FXMLLoader(getClass().getResource("/Management/Rate.fxml"));
                    Region root = null;
                    int ref = candidacyTableView.getSelectionModel().getSelectedItem().getReference();
                    String name = candidacyTableView.getSelectionModel().getSelectedItem().getStudentName();

                    try {
                        root = loader.load();

                        //Initialisation
                        RateController rateController = loader.<RateController>getController();
                        rateController.setEntityManager(entityManager);
                        rateController.getReferenceText().setText(Integer.toString(ref));
                        rateController.getStudentText().setText(name);
                        rateController.setRatingController(this);
                        rateController.init();

                        Scene scene = new Scene(root);
                        st.setScene(scene);
                        st.initModality(Modality.APPLICATION_MODAL);
                        st.setResizable(false);
                        st.setTitle("Modification d'un produit");
                        st.show();

                    } catch (IOException e) {
                        e.printStackTrace();
                    }


                }

                else {
                    //allertDialog("Erreur !", Alert.AlertType.ERROR, "Vous avez selectionné votre annonce !");

                }


            });
            return row ;
        });
    }


    public void MainPage(ActionEvent event) throws IOException {
        Stage st = new Stage();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/MainPage.fxml"));
        Region root = loader.load();

        MainPageController mainPageController = loader.<MainPageController>getController();
        mainPageController.setEntityManager(entityManager);

        //Récupération de la fenetre de login
        Node node = (Node) event.getSource();
        Stage stage =(Stage)node.getScene().getWindow();
        //Fermeture de celle-ci
        stage.close();

        Scene scene = new Scene(root);
        st.setScene(scene);
        st.initModality(Modality.APPLICATION_MODAL);
        st.setResizable(false);
        st.setTitle("Home");
        st.show();
    }





}
