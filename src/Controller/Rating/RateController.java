package Controller.Rating;

import DAO.CandidacyDAO;
import Model.Candidacy;
import Model.Student;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

import javax.persistence.EntityManager;
import java.util.List;

public class RateController {

    private RatingController ratingController;

    private CandidacyDAO candidacyDAO;

    private EntityManager entityManager;

    public void setEntityManager(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @FXML
    private Label referenceText;

    @FXML
    private Label studentText;

    @FXML
    private TextField ratingInput;

    public void init(){
        candidacyDAO = new CandidacyDAO(entityManager);
    }

    @FXML
    void rate(ActionEvent event) {
        entityManager.getTransaction().begin();
        //Ecriture dans le fichier json

        Candidacy candidacy = candidacyDAO.getObjectById(Integer.parseInt(referenceText.getText()));
        Student student = candidacy.getStudent();

        if(candidacy.getStatus().equalsIgnoreCase("Non notée")){
            int note = Integer.parseInt(ratingInput.getText());
            int newNote = (note+student.getScore());
            candidacy.setScore(newNote);
            candidacy.setStatus("En attente");
            candidacyDAO.modify(candidacy);
        }else if(candidacy.getStatus().equalsIgnoreCase("En attente")){
            int note = Integer.parseInt(ratingInput.getText());
            int newNote = (note+candidacy.getScore())/3;
            candidacy.setScore(newNote);
            candidacy.setStatus("Notée");
            candidacyDAO.modify(candidacy);
        }

        ratingController.getDonnees().clear();
        List<Candidacy> candidacyList = candidacyDAO.getAll();
        ratingController.getDonnees().addAll(candidacyList);

        //On récupère la fenêtre en cours
        Node node = (Node) event.getSource();
        Stage stage = (Stage) node.getScene().getWindow();
        //On la ferme
        stage.close();

        entityManager.getTransaction().commit();
    }

    public Label getReferenceText() {
        return referenceText;
    }

    public void setReferenceText(Label referenceText) {
        this.referenceText = referenceText;
    }

    public Label getStudentText() {
        return studentText;
    }

    public void setStudentText(Label studentText) {
        this.studentText = studentText;
    }

    public RatingController getRatingController() {
        return ratingController;
    }

    public void setRatingController(RatingController ratingController) {
        this.ratingController = ratingController;
    }
}
