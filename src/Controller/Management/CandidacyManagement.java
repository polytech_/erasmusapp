package Controller.Management;

import Controller.Add.AddCandidacy;
import Controller.MainPageController;
import DAO.CandidacyDAO;
import Model.Candidacy;
import Model.Student;
import javafx.beans.binding.Bindings;
import javafx.beans.binding.BooleanBinding;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.Region;
import javafx.stage.Modality;
import javafx.stage.Stage;

import javax.persistence.EntityManager;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class CandidacyManagement {

    private EntityManager entityManager;
    private CandidacyDAO candidacyDAO;

    private ObservableList<Candidacy> donnees;

    public ObservableList<Candidacy> getDonnees() {
        return donnees;
    }
    public void setDonnees(ObservableList<Candidacy> donnees) {
        this.donnees = donnees;
    }

    public void setEntityManager(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @FXML
    public TableView<Candidacy> candidacyTableView;

    @FXML
    public TableColumn<Candidacy, Integer> referenceColumn;

    @FXML
    public TableColumn<Candidacy, String> stdNameColumn;

    @FXML
    public TableColumn<Candidacy, Integer> ScoreColumn;

    @FXML
    public TableColumn<Candidacy, String> StatusColumn;

    public void init(){
        candidacyDAO = new CandidacyDAO(entityManager);

        referenceColumn.setCellValueFactory(new PropertyValueFactory<>("Reference"));
        ScoreColumn.setCellValueFactory(new PropertyValueFactory<>("Score"));
        StatusColumn.setCellValueFactory(new PropertyValueFactory<>("Status"));
        stdNameColumn.setCellValueFactory(new PropertyValueFactory<>("StudentName"));


        //Initialisation des données de la tableview
        setDonnees(FXCollections.observableArrayList(candidacyDAO.getAll()));

        candidacyTableView.setItems(donnees);

    }

    public void MainPage(ActionEvent event) throws IOException {
        Stage st = new Stage();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/MainPage.fxml"));
        Region root = loader.load();

        MainPageController mainPageController = loader.<MainPageController>getController();
        mainPageController.setEntityManager(entityManager);

        //Récupération de la fenetre de login
        Node node = (Node) event.getSource();
        Stage stage =(Stage)node.getScene().getWindow();
        //Fermeture de celle-ci
        stage.close();

        Scene scene = new Scene(root);
        st.setScene(scene);
        st.initModality(Modality.APPLICATION_MODAL);
        st.setResizable(false);
        st.setTitle("Home");
        st.show();
    }

    public void addCandidacy(ActionEvent event) throws IOException {
        Stage st = new Stage();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/Add/AddCandidacy.fxml"));
        Region root = loader.load();

        AddCandidacy addCandidacy = loader.getController();
        addCandidacy.setCandidacyManagement(this);
        addCandidacy.setEntityManager(entityManager);
        addCandidacy.init();

        Scene scene = new Scene(root);
        st.setScene(scene);
        st.initModality(Modality.APPLICATION_MODAL);
        st.setResizable(false);
        st.setTitle("Add Candidacy");
        st.show();
    }
}
